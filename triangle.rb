# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
  # WRITE THIS CODE
  args = [a,b,c]
  raise TriangleError if args.any?{|int| int <= 0}
  # Find the longest side
  longest = args.max
  # Delete the element from the array
  index_of_max = args.index(longest)
  args.delete_at(index_of_max)
  sum_of_other_sides = args.reduce(:+)
  raise TriangleError if sum_of_other_sides <= longest
  return :equilateral if (a == b && a == c)
  return :isosceles if (a == b || a == c || b == c)
  return :scalene if (a != b && a != c && b != c)
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
